﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.FDT.Common;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;

namespace com.FDT.GameManager
{
    [CreateAssetMenu(menuName = "FDT/GameManager/GameManagerResources", fileName = "GameManagerResources")]
    public class GameManagerResources : GameManager
    {
        protected AsyncOperation async;
        
        public void PreloadScene(string sceneName, LoadSceneMode mode)
        {
            PreloadScene(sceneName, mode, false, null);
        }

        
        public override void PreloadScene(string sceneName, LoadSceneMode mode, bool activate, Action<string> callback)
        {
            // check if scene is loading
            bool sceneLoaded = IsSceneLoaded(sceneName);

            if (sceneLoaded)
            {
                callback(sceneName);
                return;
            }

            LoadSceneData data = new LoadSceneData
            {
                sceneName = sceneName, callback = callback, loadMode = mode, activate = activate
            };
            
            pendingLoads.Add(data);
            if (!isLoadingScene)
            {
                _state = GameManagerState.PRELOADING;
                Extensions.StartCoroutine(doPreloadScene());
            }
        }
        public override bool IsSceneLoaded(string sceneName)
        {
            bool sceneLoaded = false || currentSceneLoad != null && currentSceneLoad.sceneName == sceneName;
            if (!sceneLoaded)
            {
                // test if is previously loaded
                var c = SceneManager.sceneCount;
                for (int i = 0; i < c; i++)
                {
                    var sc =SceneManager.GetSceneAt(i);
                    if (sc.name == sceneName)
                    {
                        sceneLoaded = true;
                        break;
                    }
                }
            }
            if (!sceneLoaded)
            {
                for (int i = 0; i < currentLoaded.Count; i++)
                {
                    if (currentLoaded[i].sceneName == sceneName)
                    {
                        sceneLoaded = true;
                        break;
                    }
                }
            }
            return sceneLoaded;
        }
        protected IEnumerator doPreloadScene()
        {
            _scenePreloadRequestedEvt.Raise(currentSceneLoad.sceneName);
            Debug.Log("starts preloading scene " + currentSceneLoad.sceneName);
            _sceneLoadEvt.AddEventListener(HandleSceneLoad);
            _isloading = true;
            async = SceneManager.LoadSceneAsync(currentSceneLoad.sceneName, currentSceneLoad.loadMode);
            async.allowSceneActivation = false;
            do
            {
                _scenePreloadProgressEvt.Raise(currentSceneLoad.sceneName, async.progress);
                yield return null;
            } while (async != null && async.progress < 0.9f);

            Debug.Log("ready to load");
            _state = GameManagerState.WAITING_ACTIVATION;
            _scenePreloadFinishedEvt.Raise(currentSceneLoad.sceneName);
            //_isloading = false;
            TestActivation();
        }


        public void ActivatePreloadedScene()
        {
            currentSceneLoad.activate = true;
            if (_state != GameManagerState.WAITING_ACTIVATION)
                throw new System.Exception("GameManager was asked to activate a scene and is not waiting a scene activation.");
            TestActivation();
        }
        protected void TestActivation()
        {
            if (currentSceneLoad.activate && _state == GameManagerState.WAITING_ACTIVATION)
            {
                Extensions.StartCoroutine(doActivateScene());
            }
            else
            {
                TestNext();
            }
        }

        protected void TestNext()
        {
            if (pendingLoads.Count > 0)
            {
                Extensions.StartCoroutine(doPreloadScene());
            }
        }

        private IEnumerator doActivateScene()
        {
            currentSceneLoad.activate = false;
            Debug.Log("setting async allowsceneactivation to TRUE");
            _sceneLoadEvt.RemoveEventListener(HandleSceneLoad);
            _state = GameManagerState.ACTIVATING;
            if (_isloading)
            {
                async.allowSceneActivation = true;
                do
                {
                    yield return null;
                } while (async.progress < 0.9f);
            }
            _state = GameManagerState.IDLE;
            //async = null;

            
            _isloading = false;
            string sceneName = currentSceneLoad.sceneName;
            var callback = currentSceneLoad.callback;

            if (callback!=null)
            {
                callback(sceneName);
            }
            _scenePreloadActivatedEvt.Raise(sceneName);
            currentLoaded.Add(currentSceneLoad);
            pendingLoads.RemoveAt(0);
            TestNext();
        }

        public override UnloadSceneAsyncOp UnloadSceneAsync(string currentSceneScene)
        {
            UnloadSceneAsyncOp a = new UnloadSceneAsyncOp();
            return a;
        }
    }
}
