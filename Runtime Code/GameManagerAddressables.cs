﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.FDT.Common;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;

namespace com.FDT.GameManager
{
    [CreateAssetMenu(menuName = "FDT/GameManager/GameManagerAddressables", fileName = "GameManagerAddressables")]
    public class GameManagerAddressables : GameManager
    {
        protected AsyncOperationHandle<SceneInstance> asyncAddr;
        
        public void PreloadScene(string sceneName, LoadSceneMode mode)
        {
            PreloadScene(sceneName, mode, false, null);
        }
        public override void PreloadScene(string sceneName, LoadSceneMode mode, bool activate, Action<string> callback)
        {
            // check if scene is loading
            bool sceneLoaded = IsSceneLoaded(sceneName);

            if (sceneLoaded)
            {
                callback(sceneName);
                return;
            }

            LoadSceneData data = new LoadSceneData
            {
                sceneName = sceneName, callback = callback, loadMode = mode, activate = activate
            };
            
            pendingLoads.Add(data);
            if (!isLoadingScene)
            {
                _state = GameManagerState.PRELOADING;
                Extensions.StartCoroutine(doPreloadScene());
            }
        }
        public override bool IsSceneLoaded(string sceneName)
        {
            bool sceneLoaded = false || currentSceneLoad != null && currentSceneLoad.sceneName == sceneName;
            if (!sceneLoaded)
            {
                // test if is previously loaded
                var c = SceneManager.sceneCount;
                for (int i = 0; i < c; i++)
                {
                    var sc =SceneManager.GetSceneAt(i);
                    if (sc.name == sceneName)
                    {
                        sceneLoaded = true;
                        break;
                    }
                }
            }

            if (!sceneLoaded && currentLoaded.Count>0)
            {
                for (int i = 0; i < currentLoaded.Count; i++)
                {
                    if (currentLoaded[i].sceneName == sceneName)
                    {
                        sceneLoaded = true;
                        break;
                    }
                }
            }
            return sceneLoaded;
        }
        
        protected IEnumerator doPreloadScene()
        {
            _scenePreloadRequestedEvt.Raise(currentSceneLoad.sceneName);
            Debug.Log("starts preloading scene " + currentSceneLoad.sceneName);
            _sceneLoadEvt.AddEventListener(HandleSceneLoad);
            
            _isloading = true;
            
            asyncAddr = Addressables.LoadSceneAsync(currentSceneLoad.sceneName + ".unity", currentSceneLoad.loadMode, false);

            do
            {
                _scenePreloadProgressEvt.Raise(currentSceneLoad.sceneName, asyncAddr.PercentComplete);
                yield return null;
            } while (!asyncAddr.IsDone);
        
            Debug.Log("ready to load");
            _state = GameManagerState.WAITING_ACTIVATION;
            _scenePreloadFinishedEvt.Raise(currentSceneLoad.sceneName);
            //_isloading = false;
            TestActivation();
        }

        public void ActivatePreloadedScene()
        {
            currentSceneLoad.activate = true;
            if (_state != GameManagerState.WAITING_ACTIVATION)
                throw new System.Exception("GameManager was asked to activate a scene and is not waiting a scene activation.");
            TestActivation();
        }
        protected void TestActivation()
        {
            if (currentSceneLoad.activate && _state == GameManagerState.WAITING_ACTIVATION)
            {
                Extensions.StartCoroutine(doActivateScene());
            }
            else
            {
                TestNext();
            }
        }

        private void HandleSceneLoaded(Scene s, LoadSceneMode m)
        {
            SceneManager.sceneLoaded -= HandleSceneLoaded;
            _isloading = false;
            string sceneName = currentSceneLoad.sceneName;
            var callback = currentSceneLoad.callback;

            if (callback != null)
            {
                callback(sceneName);
            }
            _scenePreloadActivatedEvt.Raise(sceneName);
            _state = GameManagerState.IDLE;
            currentLoaded.Add(currentSceneLoad);
            pendingLoads.RemoveAt(0);
            TestNext();
        }
        private IEnumerator doActivateScene()
        {
            currentSceneLoad.activate = false;
            Debug.Log("setting async allowsceneactivation to TRUE");
            _sceneLoadEvt.RemoveEventListener(HandleSceneLoad);
            _state = GameManagerState.ACTIVATING;
            currentSceneLoad.sceneInstance = asyncAddr.Result;
            if (_isloading)
            {
                do
                {
                    yield return null;
                } while (!asyncAddr.IsDone);
                asyncAddr.Result.Activate();
                SceneManager.sceneLoaded += HandleSceneLoaded;
            }
        }
        
        protected void TestNext()
        {
            if (pendingLoads.Count > 0)
            {
                Extensions.StartCoroutine(doPreloadScene());
            }
        }
        public override UnloadSceneAsyncOp UnloadSceneAsync(string currentSceneScene)
        {
            UnloadSceneAsyncOp a = new UnloadSceneAsyncOp();
            bool isDone = false;
            for (int i = currentLoaded.Count-1 ; i >= 0; i--)
            {
                if (currentLoaded[i].sceneName == currentSceneScene)
                {
                    Addressables.UnloadSceneAsync(currentLoaded[i].sceneInstance, true).Completed += handle =>
                    {
                        isDone = true;
                        a.IsDone = true;
                    };
                    currentLoaded.RemoveAt(i);
                }
            }

            if (!isDone)
            {
                Debug.LogWarning($"Scene {currentSceneScene} not found to unload from Addressables");
                a.IsDone = true;
            }
            return a;
        }
        

    }
}
