using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.GameManager
{
	public class ScenePreloadStringGameEventListener : GameEvent1Listener<string, ScenePreloadStringGameEvent, ScenePreloadStringGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<string>
        {
            
        }
	}
}
