using com.FDT.GameEvents;

namespace com.FDT.GameManager
{
	[System.Serializable]
	public class ScenePreloadStringGameEventHandle: GameEvent1Handle<string, ScenePreloadStringGameEvent>
	{
	}
}
