using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.GameManager
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/GameManager/ScenePreloadActivatedGameEvent")]
	public class ScenePreloadStringGameEvent : GameEvent1<string>
	{
        public override string arg0label
        {
            get { return "Scene"; }
        }
    }
}
