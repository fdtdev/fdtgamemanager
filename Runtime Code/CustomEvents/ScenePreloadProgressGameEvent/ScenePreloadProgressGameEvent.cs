using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.GameManager
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/GameManager/ScenePreloadProgressGameEvent")]
	public class ScenePreloadProgressGameEvent : GameEvent2<string, float>
	{
        public override string arg0label
        {
            get { return "Scene"; }
        }

        public override string arg1label
        {
            get { return "Progress"; }
        }
    }
}
