using com.FDT.GameEvents;

namespace com.FDT.GameManager
{
	[System.Serializable]
	public class ScenePreloadProgressGameEventHandle: GameEvent2Handle<string, float, ScenePreloadProgressGameEvent>
	{
	}
}
