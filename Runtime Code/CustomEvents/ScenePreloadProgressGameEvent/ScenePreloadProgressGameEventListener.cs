using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.GameManager
{
	public class ScenePreloadProgressGameEventListener : GameEvent2Listener<string, float, ScenePreloadProgressGameEvent, ScenePreloadProgressGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<string, float>
        {
        }
	}
}
