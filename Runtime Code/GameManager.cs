﻿
using System;
using System.Collections.Generic;
using com.FDT.Common;
using com.FDT.Common.ReloadedScriptableObject;
using com.FDT.SystemEventsHandler;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace com.FDT.GameManager
{

    public abstract class GameManager : ScriptableObjectWithDescription
    {
        public enum GameManagerState
        {
            IDLE = 0, PRELOADING = 1, WAITING_ACTIVATION = 2, ACTIVATING = 3
        }

        protected LoadSceneData currentSceneLoad
        {
            get
            {
                if (pendingLoads.Count>0)
                    return pendingLoads[0];
                return null;
            }
        }
        
        protected GameManagerState _state;
        public GameManagerState state { get { return _state; } }
        
        [Header("GameEvents"), SerializeField] protected ScenePreloadStringGameEvent _scenePreloadRequestedEvt;
        [SerializeField] protected ScenePreloadStringGameEvent _scenePreloadFinishedEvt;
        [SerializeField] protected ScenePreloadStringGameEvent _scenePreloadActivatedEvt;
        [SerializeField] protected ScenePreloadProgressGameEvent _scenePreloadProgressEvt;
        [SerializeField] protected SceneLoadGameEventHandle _sceneLoadEvt = default(SceneLoadGameEventHandle) ;
        
        protected bool _isloading = false;
        public bool isLoadingScene { get { return _isloading; } }
        
        public List<LoadSceneData> pendingLoads = new List<LoadSceneData>();
        public List<LoadSceneData> currentLoaded = new List<LoadSceneData>();
        
        [System.Serializable]
        public class LoadSceneData
        {
            public string sceneName;
            public SceneInstance sceneInstance;
            public LoadSceneMode loadMode;
            public AsyncOperationHandle<SceneInstance> asyncAddr;
            public Action<string> callback;
            public bool activate = false;
            
        }
#if UNITY_EDITOR
        public override void ResetData()
        {
            base.ResetData();
            _state = GameManagerState.IDLE;
            _isloading = false;
            pendingLoads.Clear();
            currentLoaded.Clear();
        }
#endif
        public virtual void OnEnable()
        {
            _state = GameManagerState.IDLE;
            pendingLoads.Clear();
            currentLoaded.Clear();
            
        }
        public virtual void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                _sceneLoadEvt.RemoveEventListener(HandleSceneLoad);    
            }
        }
        public void HandleSceneLoad(Scene s, LoadSceneMode m)
        {
            Debug.Log(string.Format("Loaded {0}  {1}", s.name, m));
            //async = null;
        }
        public void PreloadAndActivateScene(string sceneName, LoadSceneMode mode)
        {
            PreloadScene(sceneName, mode, true, null);
        }
        public void PreloadAndActivateScene(string sceneName, LoadSceneMode mode, Action<string> callback)
        {
            PreloadScene(sceneName, mode, true, callback);
        }

        public virtual void PreloadScene(string sceneName, LoadSceneMode mode, bool activate, Action<string> callback)
        {
        }

        public virtual bool IsSceneLoaded(string sceneName)
        {
            return false;
        }
        public virtual UnloadSceneAsyncOp UnloadSceneAsync(string currentSceneScene)
        {
            return null;
        }
        public class UnloadSceneAsyncOp:AsyncOpBase<UnloadSceneAsyncOp>
        {
            
        }
    }

}
