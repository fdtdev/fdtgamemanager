using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.GameManager.Editor
{
	[CustomEditor(typeof(ScenePreloadStringGameEvent))]
	public class ScenePreloadStringGameEventEditor : GameEvent1Editor<string>
	{
	}
}
