using UnityEngine;
using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.GameManager.Editor
{
	[CustomEditor(typeof(ScenePreloadProgressGameEvent))]
	public class ScenePreloadProgressGameEventEditor : GameEvent2Editor<string, float>
	{
	}
}
