# FDT Game Manager

System Events Handler Addon to control scenes loading


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.gamemanager": "https://bitbucket.org/fdtdev/fdtgamemanager.git#3.1.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.2.0",
	"com.fdt.systemeventshandler": "https://bitbucket.org/fdtdev/fdtsystemeventshandler.git#3.0.0",
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.3.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtgamemanager/src/3.1.0/LICENSE.md)